---
title: "Muhammad Subianto Website"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Administrative

- Syllabus

# Lectures

- 00 Likert
  - Analysis and Visualization of Likert Based Items [GitHub](https://github.com/jbryer/likert).
  - On Likert Scales In R [GitHub](https://jakec007.github.io/2021-06-23-R-likert/), [GitHub](https://github.com/JakeC007/likert-R-examples).
  - Likert Scale: Definition, Examples, and Visualization [GitHub](https://jtr13.github.io/cc20/likert-scale-definition-examples-and-visualization.html).
  - Likert RPubs [RPubs](https://rpubs.com/nicokyle/932333).
  - Visualize Likert Scales (Good) [RPubs](https://rpubs.com/chidungkt/928081).
  - LikertMakeR [RPubs](https://rpubs.com/WinzarH/980978).
- 01 K-Medoids
  - Simple K-Medoids Partitioning Algorithm for Mixed Variable Data [paper](https://www.mdpi.com/1999-4893/12/9/177).
- 02 Web
  - Data Scraping from Web [RPubs](https://rpubs.com/chidungkt/810760).
  - Lecture 02-2: Probability and simulation.
- 03 Simple linear regression
  - Lecture 03-1: The Model. 
      - Chapter 1 of KNNL.
- 06 Logistic Regression
  - Lecture 06-1: Logistic Regression
    - Chapter 20 of Statistical Sleuth
    - Sections 14.1–14.10 of KNNL
- 07 Clustering Algorithms
    - Clustering Algorithms: A Real-world Case [RPubs](https://rpubs.com/chidungkt/808614).
    - Hierarchical Clustering [RPubs](https://rpubs.com/chidungkt/707528).
    - Market Segmentation: K-means Clustering Algorithm [RPubs](https://rpubs.com/chidungkt/539750).
- 08 ROC Curve
    - Understand ROC Curve and Search Threshold that maximizes Profit [RPubs](https://rpubs.com/chidungkt/676666).
- 09 Structural Equation Modeling (SEM)
    - R for Psychologists and Marketing Research: Structural Equation Modeling (SEM) [RPubs](https://rpubs.com/chidungkt/506708).
- 10 Convolutional Neural Network (CNN)
    - Convolutional Neural Network (CNN) for Predicting Patients Infected Covid 19 from CT Scan Image [RPubs](https://rpubs.com/chidungkt/855459).
    
